function toSemVer($version){
    $version -match "^.*?(?<major>\d+)(\.(?<minor>\d+))?(\.(?<patch>\d+))?(\-(?<pre>[0-9A-Za-z\-\.]+))?(\+(?<build>[0-9A-Za-z\-\.]+))?$" | Out-Null
    $major = [int]$matches['major']
    $minor = [int]$matches['minor']
    $patch = [int]$matches['patch']
    
    if($null -eq $matches['pre']){$pre = @()}
    else{$pre = $matches['pre'].Split(".")}

    New-Object PSObject -Property @{ 
        Major = $major
        Minor = $minor
        Patch = $patch
        Pre = $pre
        VersionString = $version
    }
}

function compareSemVer($a, $b){
    $result = 0
    $result =  $a.Major.CompareTo($b.Major)
    if($result -ne 0){return $result}

    $result = $a.Minor.CompareTo($b.Minor)
    if($result -ne 0){return $result}

    $result = $a.Patch.CompareTo($b.Patch)
    if($result -ne 0){return $result}
    $ap = $a.Pre
    $bp = $b.Pre
    if($ap.Length  -eq 0 -and $bp.Length -eq 0) {return 0}
    if($ap.Length  -eq 0){return 1}
    if($bp.Length  -eq 0){return -1}
    
    $minLength = [Math]::Min($ap.Length, $bp.Length)
    for($i = 0; $i -lt $minLength; $i++){
        $ac = $ap[$i]
        $bc = $bp[$i]

        $anum = 0 
        $bnum = 0
        $aIsNum = [Int]::TryParse($ac, [ref] $anum)
        $bIsNum = [Int]::TryParse($bc, [ref] $bnum)

        if($aIsNum -and $bIsNum) { 
            $result = $anum.CompareTo($bnum) 
            if($result -ne 0)
            {
                return $result
            }
        }
        if($aIsNum) {
            return -1
        }
        if($bIsNum) {
            return 1
        }
        $result = [string]::CompareOrdinal($ac, $bc)
        if($result -ne 0) {
            return $result
        }
    }
    return $ap.Length.CompareTo($bp.Length)
}

function rankedSemVer($versions){
    
    for($i = 0; $i -lt $versions.Length; $i++){
        $rank = 0
        for($j = 0; $j -lt $versions.Length; $j++){
            $diff = 0
            $diff = compareSemVer $versions[$i] $versions[$j]
            if($diff -gt 0) {
                $rank++
            }
        }
        $current = [PsObject]$versions[$i]
        Add-Member -InputObject $current -MemberType NoteProperty -Name Rank -Value $rank
    }
    return $versions
}
